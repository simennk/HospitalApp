import hospital.Department;
import hospital.Employee;
import hospital.Patient;
import hospital.exception.RemoveException;
import org.junit.jupiter.api.*;

import java.lang.reflect.Constructor;
import java.lang.reflect.InvocationTargetException;

import static org.junit.jupiter.api.Assertions.*;

public class DepartmentTest{
    Department testDepartment;
    Employee testEmployee1;
    Employee testEmployee2;


    Patient testPatient1;
    Patient testPatient2;




    @BeforeEach
    public void testSetUp(){
        testDepartment = new Department("test");
        testEmployee1 = new Employee("1","1","1");
        testEmployee2 = new Employee("2","2","2");
        testDepartment.addEmployee(testEmployee1);
        testDepartment.addEmployee(testEmployee2);

        //Seeing as patient is protected, there are a few things that need to be done to create objects of the class
        Class pat = hospital.Patient.class;
        Constructor pat_init;

        {
            try {
                pat_init = pat.getDeclaredConstructor(String.class, String.class, String.class);
                pat_init.setAccessible(true);
                testPatient1 = (Patient) pat_init.newInstance( "3", "3", "3");
                testPatient2 = (Patient) pat_init.newInstance( "4", "4", "4");
            } catch (NoSuchMethodException | IllegalAccessException | InvocationTargetException | InstantiationException e) {
                System.out.println("This is not supposed to happen");
                e.printStackTrace();
            }
        }
        testDepartment.addPatient(testPatient1);
        testDepartment.addPatient(testPatient2);
    }

    @DisplayName("Tests to see that employees are removed correctly")
    @Test
    public void testRemoveEmployee(){
            assertEquals(2, testDepartment.getEmployees().size());
            try {
                testDepartment.remove(testEmployee1);
            } catch(RemoveException rE){
                assertEquals(1,0);
            }
            assertFalse(testDepartment.getEmployees().contains(testEmployee1));
            assertEquals(1,testDepartment.getEmployees().size());
    }

    @DisplayName("Attempts to remove nonexistent object")
    @Test
    public void removeNonExisting(){
        boolean flag = false;
        assertEquals(2, testDepartment.getEmployees().size());
        try {
            testDepartment.remove(null);
        } catch(RemoveException rE){
            //An exception is expected to be thrown in this instance
            flag = true;
        }
        assertEquals(2,testDepartment.getEmployees().size());
        assertTrue(flag);
    }


    @DisplayName("Tests to see that patients are removed correctly")
    @Test
    public void testRemovePatient(){
        assertEquals(2, testDepartment.getPatients().size());
        try {
            testDepartment.remove(testPatient1);
        } catch(RemoveException rE){
            assertEquals(1,0);
        }
        assertFalse(testDepartment.getPatients().contains(testPatient1));
        assertEquals(1,testDepartment.getPatients().size());
    }



}