package hospital;

import hospital.Person;
import hospital.exception.RemoveException;

import java.util.ArrayList;
import java.util.Iterator;
import java.util.List;
import java.util.Objects;

public class Department {
    private String departmentName;
    private ArrayList<Employee> employees;
    private ArrayList<Patient> patients;

    /**
     * Creates an object of Department
     * @param departmentName the name of the department, which cannot be null or blank
     * @throws IllegalArgumentException This is thrown if department name is null or blank
     */
    public Department(String departmentName) {
        if (departmentName == null || departmentName.isBlank()){
            throw new IllegalArgumentException("Department name cannot be blank or null");
        } else {
            this.departmentName = departmentName;
            employees = new ArrayList<>();
            patients = new ArrayList<>();
        }
    }

    /**
     * Returns the name of the department
     * @return
     */
    public String getDepartmentName() {
        return departmentName;
    }

    /**
     * Sets the name of the department
     * @param departmentName
     */
    public void setDepartmentName(String departmentName) {
        this.departmentName = departmentName;
    }

    /**
     * Returns a list containing all employees assigned to the department
     * @return
     */
    public List<Employee> getEmployees() {
        return employees;
    }

    /**
     * Returns a list containing all patients assigned to the department
     * @return
     */
    public List<Patient> getPatients() {
        return patients;
    }

    /**
     * Adds an employee object to the employees ArrayList
     * @param employee The employee to be added, cannot already be in the list or null
     */
    public void addEmployee(Employee employee){
        //This is made under the assumption that an employee cannot have multiple jobs, meaning an employee can only be listed
        //as nurse or surgeon, but not both. This method should preferably be a boolean to signify wether the employee was added
        //or not, but in accordance with the class diagram this change will not be made
        if (employee == null){
            throw new IllegalArgumentException("Employee cannot be null");
        }
        else{
        boolean valid = true;
        Iterator<Employee> it = employees.iterator();
        while (it.hasNext()){
            if(it.next().getPersonnummer().equals(employee.getPersonnummer())){
                valid = false;
            }
        }
        if (valid) {
            employees.add(employee);
        }
        }
    }

    /**
     * Adds a patient object to the patients ArrayList
     * @param patient The patient to be added, cannot already be in the list or null
     */
    public void addPatient(Patient patient){
        //I assume that the same patient cannot be registered twice, if one would like to have multiple diagnoses for a certain
        //patient, the diagnosis field in patient should be an ArrayList or something of the sort
        if (patient == null){
            throw new IllegalArgumentException("Patient cannot be null");
        }
        else{
        boolean valid = true;
        Iterator<Patient> it = patients.iterator();
        while (it.hasNext()){
            if(it.next().getPersonnummer().equals(patient.getPersonnummer())){
                valid = false;
            }
        }
        if(valid) {
            patients.add(patient);
        }
        }
    }

    /**
     * Removes a person from one of the lists
     * @param person The person to be removed
     * @throws RemoveException This exception is thrown if the person does not exist in either list
     */
    public void remove(Person person) throws RemoveException {
        //This assumes that a person cannot be both a patient and an employee
        if (employees.contains(person)){
            employees.remove(person);
        }
        else if(patients.contains(person)){
            patients.remove(person);
        }
        else {
            throw new RemoveException("The object did not exist in either of the Lists");
        }
    }

    /**
     * Compares two objects of Department to see if they are equal, departmentName cannot be similar for two departments
     * @param o
     * @return
     */
    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;

        Department that = (Department) o;

        return departmentName.equals(that.departmentName);
    }


    /**
     * Generates hashcode
     * @return
     */
    @Override
    public int hashCode() {
        return departmentName.hashCode();
    }

    /**
     * Returns the information stored in the object as a String
     * @return
     */
    @Override
    public String toString() {
        return departmentName;
    }
}
