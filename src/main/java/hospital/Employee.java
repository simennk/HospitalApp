package hospital;

import hospital.Person;

public class Employee extends Person {

    /**
     * Creates an object of Employee
     * @param firstName
     * @param lastName
     * @param socialSecurityNumber
     */
    public Employee(String firstName, String lastName, String socialSecurityNumber){
        super(firstName, lastName, socialSecurityNumber);
    }

    /**
     * Returns the information stored in the object as a String
     * @return
     */
    @Override
    public String toString(){
        return super.toString() + ", This is an employee";
    }

}
