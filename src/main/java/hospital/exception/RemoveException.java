package hospital.exception;

public class RemoveException extends Exception {
    private static final long serialVersionUID = 1L;

    /**
     * Creates an object of RemoveException
     * @param errorMessage The nature of the error
     */
    public RemoveException(String errorMessage){
        super(errorMessage);
    }
}
