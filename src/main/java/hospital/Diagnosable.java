package hospital;

public interface Diagnosable {
    /**
     * Ensures that the Patient class includes this method
     * @param diagnosis
     */
    public void setDiagnosis(String diagnosis);
}
