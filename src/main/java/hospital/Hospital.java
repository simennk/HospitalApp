package hospital;

import hospital.Department;

import java.util.ArrayList;
import java.util.Iterator;
import java.util.List;

public class Hospital {
    private final String hospitalName;

    private ArrayList<Department> departments;

    /**
     * Creates an object of Hospital
     * @param hospitalName The name of the hospital, cannot be null or blank
     * @throws IllegalArgumentException This is thrown if the name is null or blank
     */
    public Hospital(String hospitalName) {
        if (hospitalName == null || hospitalName.isBlank()) {
            throw new IllegalArgumentException("Hospital name cannot be blank or zero");
        } else {
            this.hospitalName = hospitalName;
            departments = new ArrayList<>();
        }
    }

    /**
     * Returns the name of the hospital
     * @return
     */
    public String getHospitalName() {
        return hospitalName;
    }

    /**
     * Adds a department to the departments ArrayList
     * @param department the department to be added, cannot already exist in departments or be null
     */
    public void addDepartment(Department department){
        if (department == null){
            throw new IllegalArgumentException("Department cannot be null");
        }
        else{
        boolean valid = true;
        Iterator<Department> it= departments.iterator();
        while (it.hasNext()){
            if (it.next().equals(department)){
                valid = false;
            }
        }
        if (valid) {
            departments.add(department);
            }
        }
    }

    /**
     * Returns a List containing all departments stored in the departments ArrayList
     * @return
     */
    public List<Department> getDepartments(){
        return departments;
    }

    /**
     * Returns the information stored in hospital as a String
     * @return
     */
    public String toString(){
        return hospitalName;
    }

}
