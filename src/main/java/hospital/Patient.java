package hospital;

import hospital.Diagnosable;
import hospital.Person;

public class Patient extends Person implements Diagnosable {
    private String diagnosis = "";

    /**
     * Creates an object of Patient
     * @param firstName
     * @param lastName
     * @param socialSecurityNumber
     */
    protected Patient(String firstName, String lastName, String socialSecurityNumber){
        super(firstName, lastName, socialSecurityNumber);
    }

    /**
     * Returns the diagnosis assigned to the patient
     * @return
     */
    protected String getDiagnose() {
        return diagnosis;
    }

    /**
     * Sets a diagnosis on the patient
     * @param diagnosis
     */
    @Override
    public void setDiagnosis(String diagnosis) {
        this.diagnosis = diagnosis;
    }

    /**
     * Returns all the information stored in patient as a String
     * @return
     */
    @Override
    public String toString() {
        return "hospital.Patient{" +
                "firstName='" + this.getFirtName() + '\'' +
                ", lastName='" + this.getLastName() + '\'' +
                ", socialSecurityNumber='" + this.getPersonnummer() + '\'' +
                ", diagnosis='" + diagnosis + '\'' +
                '}';
    }
}
