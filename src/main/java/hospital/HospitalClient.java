package hospital;

import hospital.exception.RemoveException;
import hospital.healthpersonal.Nurse;
import hospital.healthpersonal.doctor.Surgeon;

public class HospitalClient {

    //Multiple tests to ensure that the program does not crash under normal use
    public static void main(String[] args) {

        Hospital hospital = new Hospital("Hamar sykehus");
        //Ensuring that get with an empty list still works
        System.out.println(hospital.getDepartments());
        System.out.println(hospital.getDepartments().size());
        HospitalTestData.fillRegisterWithTestData(hospital);

        //Attempts to remove an object in employees
        System.out.println(hospital.getDepartments().get(0).getEmployees().get(0));
        try {
            hospital.getDepartments().get(0).remove(hospital.getDepartments().get(0).getEmployees().get(0));
        } catch (RemoveException removeException){
            System.out.println("Something is wrong");
        }
        System.out.println(hospital.getDepartments().get(0).getEmployees().get(0));

        //Attempts to remove an object that does not exist
        try {
            hospital.getDepartments().get(0).remove(null);
        } catch (RemoveException removeException){
            removeException.printStackTrace();
            System.out.println("This is expected to happen");
        }
        System.out.println("The program does not abort");

        //Tests whether surgeons can set a diagnosis
        Surgeon surgeon = (Surgeon) hospital.getDepartments().get(0).getEmployees().get(3);
        surgeon.setDiagnosis(hospital.getDepartments().get(0).getPatients().get(0), "Lumbago");
        System.out.println(hospital.getDepartments().get(0).getPatients().get(0).toString());
        System.out.println(hospital.getDepartments().get(0).getEmployees().get(4).toString());
        System.out.println(hospital.getDepartments().get(0).getEmployees().get(4).getFullNavn());


        //Test to see if departments with the same name can be added
        Department testDepartment1 = new Department("test");
        Department testDepartment2 = new Department("test");
        System.out.println(hospital.getDepartments().size() + " This number should increase by 1");
        hospital.addDepartment(testDepartment1);
        hospital.addDepartment(testDepartment2);
        System.out.println(hospital.getDepartments().size() + " This should be 1 greater than the previous number");

        //Attempts to add objects with illegal arguments
        try {
            Hospital testHospital = new Hospital(null);
        }
        catch (IllegalArgumentException iAE){
            iAE.printStackTrace();
        }

        try {
            Nurse testNurse = new Nurse(null, "", "");
        }
        catch (IllegalArgumentException iAE){
            iAE.printStackTrace();
        }
        try{
            Department testDepartment = new Department("");
        } catch (IllegalArgumentException iAE){
            iAE.printStackTrace();
        }
        try{
            hospital.getDepartments().get(0).addEmployee(null);
        } catch (IllegalArgumentException iAE){
            iAE.printStackTrace();
        }
        try{
            hospital.getDepartments().get(0).addPatient(null);
        } catch (IllegalArgumentException iAE){
            iAE.printStackTrace();
        }
        try{
            hospital.addDepartment(null);
        } catch (IllegalArgumentException iAE){
            iAE.printStackTrace();
        }
        System.out.println("The program runs after an illegal argument so long as it is caught");


        //Tests to see that adding multiple patients and employees with the same SSN does not crash the program
        Patient testPatient1 = new Patient("pat1","pat1","pat1");
        Patient testPatient2 = new Patient("pat2","pat2","pat1");
        System.out.println(hospital.getDepartments().get(0).getPatients().size() + " This number should increase by 1");
        hospital.getDepartments().get(0).addPatient(testPatient1);
        hospital.getDepartments().get(0).addPatient(testPatient2);
        System.out.println(hospital.getDepartments().get(0).getPatients().size() + " This number should have increased by 1");

        Nurse testEmployee1 = new Nurse("em1", "em1", "em1");
        Surgeon testEmployee2 = new Surgeon("em2", "em2", "em1");
        System.out.println(hospital.getDepartments().get(0).getEmployees().size() + " This number should increase by 1");
        hospital.getDepartments().get(0).addEmployee(testEmployee1);
        hospital.getDepartments().get(0).addEmployee(testEmployee2);
        System.out.println(hospital.getDepartments().get(0).getEmployees().size() + " This number should have increased by 1");

    }



}
