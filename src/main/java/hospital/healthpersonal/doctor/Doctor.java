package hospital.healthpersonal.doctor;

import hospital.Employee;
import hospital.Patient;

public abstract class Doctor extends Employee {

    /**
     * Used by GeneralPractitioner and Surgeon to reach the Person constructor
     * @param firstName
     * @param lastName
     * @param socialSecurityNumber
     */
    protected Doctor(String firstName, String lastName, String socialSecurityNumber){
        super(firstName, lastName, socialSecurityNumber);
    }

    /**
     * Sets a diagnosis for a specified patient
     * @param patient The patient to be diagnosed
     * @param diagnosis The diagnosis to be assigned
     */
    public abstract void setDiagnosis(Patient patient, String diagnosis);

}
