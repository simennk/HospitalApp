package hospital.healthpersonal.doctor;

import hospital.Patient;

public class Surgeon extends Doctor {

    /**
     * Creates an object of Surgeon
     * @param firstName
     * @param lastName
     * @param socialSecurityNumber
     */
    public Surgeon(String firstName, String lastName, String socialSecurityNumber){
        super(firstName, lastName, socialSecurityNumber);
    }

    /**
     * Sets a diagnosis for a specified patient
     * @param patient The patient to be diagnosed
     * @param diagnosis The diagnosis to be assigned
     */
    @Override
    public void setDiagnosis(Patient patient, String diagnosis){
        patient.setDiagnosis(diagnosis);
    }
}
