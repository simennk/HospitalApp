package hospital.healthpersonal;

import hospital.Employee;

public class Nurse extends Employee {

    /**
     * Creates an object of Nurse
     * @param firstName
     * @param lastName
     * @param socialSecurityNumber
     */
    public Nurse(String firstName, String lastName, String socialSecurityNumber){
        super(firstName, lastName, socialSecurityNumber);
    }

    /**
     * Returns the information in the object as a String
     * @return
     */
    @Override
    public String toString(){
        return super.toString() + ", This is a Nurse";
    }
}
