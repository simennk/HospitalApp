package hospital;

public abstract class Person {
    private String firstName;
    private String lastName;
    private String socialSecurityNumber;

    /**
     * Utilized by all human classes to create an object of the certain class, none of the parameters can be blank or null
     * @param firstName
     * @param lastName
     * @param socialSecurityNumber
     */
    public Person(String firstName, String lastName, String socialSecurityNumber) {
        if(firstName == null || lastName == null || socialSecurityNumber == null || firstName.isBlank() || lastName.isBlank() || socialSecurityNumber.isBlank()){
            throw new IllegalArgumentException("One or more of the parameters for Person were blank or zero");
        } else {
            this.firstName = firstName;
            this.lastName = lastName;
            this.socialSecurityNumber = socialSecurityNumber;
        }
    }

    /**
     * Returns the first and last name of a specified Person
     * @return
     */
    public String getFullNavn(){
        return firstName + " " + lastName;
    }

    //I am aware that this is a spelling error, but we were told to not deviate from the method-names and i have therefore
    //chosen too keep it as is

    /**
     * Returns the first name of a person
     * @return
     */
    public String getFirtName() {
        return firstName;
    }

    /**
     * Sets the first name of a person
     * @param firstName
     */
    public void setFirstName(String firstName) {
            this.firstName = firstName;
    }

    /**
     * Returns the last name of a person
     * @return
     */
    public String getLastName() {
        return lastName;
    }

    /**
     * Sets the last name of a person
     * @param lastName
     */
    public void setLastName(String lastName) {
        this.lastName = lastName;
    }

    /**
     * Returns the social security number of a person
     * @return
     */
    public String getPersonnummer() {
        return socialSecurityNumber;
    }

    /**
     * Sets the social security number of a person
     * @param socialSecurityNumber
     */
    public void setPersonnummer(String socialSecurityNumber) {
        this.socialSecurityNumber = socialSecurityNumber;
    }

    /**
     * Returns the information stored in the person object as a String
     * @return
     */
    @Override
    public String toString() {
        return "hospital.Person{" +
                "firstName='" + firstName + '\'' +
                ", lastName='" + lastName + '\'' +
                ", socialSecurityNumber='" + socialSecurityNumber + '\'' +
                '}';
    }
}
